import random
import math
import time

from fire import Fire
import matplotlib.pyplot as plt
from matplotlib.ticker import MaxNLocator

def RandomSolution(L):
    return [1 if random.random() >= 0.5 else -1 for _ in range(L)]


def eval_psl(coord):
    return max([(abs(sum([coord[i]*coord[i+k] for i in range(len(coord)-k)]))) for k in range(1,len(coord))])


def eval_energy(coord):
    return sum([(pow(sum([coord[i]*coord[i+k] for i in range(len(coord)-k)]),2)) for k in range(1,len(coord))])


def lssOrel(L, valueTarget, max_time, eval_fn=eval_energy):
    """
    Run lssOrel for given sequence length and constraints.

    Args:
        L (int): sequence length.
        valueTarget (int): desired metric value; the algorithm will
            search for sequences with metric less or equal to
            valueTarget.
        max_time (float): when the calculation should be stopped,
            given as Unix time.
        eval_fn (function): should return metric value for given sequence.
    """
    closePivots = [] # storage of pivots – hash table
    coord = RandomSolution(L) # initialize coordinate
    value = eval_fn(coord) # evaluations
    if value <= valueTarget: # stopping Criteria
        return (coord,value)
    while value > valueTarget :
        for k in range(4*(L+1)): # self-avoiding walk
            iterVal = 2*L
            iterCoord = coord
            for i in range(int((L+1)/2+1)): # search neighborhood
                S = coord
                # sFlipped = S.flip(i) # flip i-th bit
                sFlipped = S.copy()
                sFlipped[i] = sFlipped[i]*(-1)
                if sFlipped in closePivots:
                    continue # skip if already pivot
                else:
                    value = eval_fn(sFlipped)
                if value <= valueTarget : # stopping criteria
                    return (sFlipped, value)
                elif value <= iterVal : # random tie if equals
                    (iterVal, iterCoord) = (value, sFlipped)
            closePivots.append(coord)
            coord = iterCoord # next pivot
        if value > valueTarget:
            coord = RandomSolution(L) # reinitialize coord.
        value = eval_fn(coord) # evaluation
        if max_time < time.time():
            return (coord,value)
    return (coord,value)


def run_experiment(L, duration, target_multiplier, use_psl=False):
    """
    Run lssOrel as many times as it's possible in given
    time limit.

    Args:
        L (int): sequence length.
        duration (float): time limit in seconds.
        target_multiplier (float): multiplier of starting
            target value.
        use_psl (boolean, default False): if compute PSL metric
            (energy otherwise).

    Returns:
        List of tuples (achieved value, result, time since start).
    """

    scores = []

    start_time = time.time()
    target = target_multiplier * L

    max_time = start_time + duration
    current_time = 0.0

    eval_fn = eval_psl if use_psl else eval_energy

    while duration > current_time:
        (result, target) = lssOrel(L, target, max_time, eval_fn)

        current_time = time.time() - start_time
        score = (target, result, current_time)
        print(score)
        scores.append(score)

        target = target - 1

    return scores


def draw_plot(L, multiplier, duration, scores, filename):
    factors, _, times = tuple(zip(*scores))
    if factors[-1] > factors[-2]:
        factors = factors[:-1]
        times = times[:-1]
    
    ax = plt.gca()
    ax.yaxis.set_major_locator(MaxNLocator(integer=True))
    ax.plot(times, factors, 'o')
    ax.grid(linestyle='--')
    plt.title(f'lssOrel results for L={L} and starting target={multiplier}*L')
    plt.xlabel('time from experiment start [s]')
    plt.ylabel('target')
    plt.xlim([0, duration])
    plt.savefig(filename)

def run_all(L, multiplier, duration, filename, use_psl=False):
    scores = run_experiment(L, duration, multiplier, use_psl)
    draw_plot(L, multiplier, duration, scores, filename)

if __name__ == '__main__':
    Fire(run_all)
    
